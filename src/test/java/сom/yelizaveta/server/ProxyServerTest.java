package сom.yelizaveta.server;

import org.junit.Before;
import org.junit.Test;
import сom.yelizaveta.model.Entry;
import сom.yelizaveta.model.Query;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ProxyServerTest {


    private ProxyServer proxyServer;

    private List<Entry> resultList = new ArrayList<>();

    @Before
    public void setUp() {
        proxyServer = new ProxyServer();
        List<Query> queries = new ArrayList<>();
        queries.add(new Query("123", "234", LocalDate.now()));
        queries.add(new Query("258", "111", LocalDate.now()));
        queries.add(new Query("123", "234", LocalDate.now()));
        proxyServer.setQueries(queries);
        resultList.add(new Entry("123", 2));
        resultList.add(new Entry("258", 1));
    }

    @Test
    public void shouldReturnAdress() {

        assertEquals("234", proxyServer.getTheMostPopularAdress());
    }

    @Test(expected = NoSuchElementException.class)
    public void shouldReturnEmptyAdress() {
        ProxyServer proxyServer = new ProxyServer();
        proxyServer.getTheMostPopularAdress();
    }

    @Test
    public void shouldReturn2_When123Passed() {
        assertEquals(2, proxyServer.countOfRequestByAdressAndLastDay("123"));
    }


    @Test
    public void shouldReturnValidList() {
        List<Entry> res = proxyServer.countOfQueriesByAdresses();
        assertTrue(res.containsAll(resultList));
        assertEquals(res.size(), resultList.size());
    }
}