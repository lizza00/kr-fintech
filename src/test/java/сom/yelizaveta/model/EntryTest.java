package сom.yelizaveta.model;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class EntryTest {

    private static final int BIGGER_VALUE = 5;
    private static final int SMALLER_VALUE = 2;
    private static final int POSITIVE_RESULT = 1;
    private static final int NEGATIVE_RESULT = -1;
    private static final int ZERO_RESULT = 0;
    private Entry entryFirst = new Entry();
    private Entry entrySecond = new Entry();

    @Test
    public void shouldReturnPositiveValue() {
        entryFirst.setValue(BIGGER_VALUE);
        entrySecond.setValue(SMALLER_VALUE);
        assertEquals(POSITIVE_RESULT, entryFirst.compareTo(entrySecond));
    }

    @Test
    public void shouldReturnNegativeValue() {
        entryFirst.setValue(SMALLER_VALUE);
        entrySecond.setValue(BIGGER_VALUE);
        assertEquals(NEGATIVE_RESULT, entryFirst.compareTo(entrySecond));
    }

    @Test
    public void shouldReturnZero() {
        entryFirst.setValue(BIGGER_VALUE);
        entrySecond.setValue(BIGGER_VALUE);
        assertEquals(ZERO_RESULT, entryFirst.compareTo(entrySecond));
    }

}