package сom.yelizaveta.model;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import сom.yelizaveta.exception.ServerIsNotAvailable;
import сom.yelizaveta.security.Security;
import сom.yelizaveta.server.Server;


public class SecurityTest {

    @Mock
    private Server server = Mockito.mock(Server.class);

    private Security security = new Security(server);

    private Query query = new Query();

    @Test
    public void shouldSendRequestSend() throws ServerIsNotAvailable {
        Mockito.when(server.isServerAvailable()).thenReturn(true);
        security.sendRequest(query);
        Mockito.verify(server).sendRequest(query);
    }

    @Test(expected = ServerIsNotAvailable.class)
    public void shouldThrowExceptionSend() throws ServerIsNotAvailable {
        Mockito.when(server.isServerAvailable()).thenReturn(false);
        security.sendRequest(query);
    }

    @Test
    public void shouldSendRequestGet() throws ServerIsNotAvailable {
        Mockito.when(server.isServerAvailable()).thenReturn(true);
        security.getRequest(query);
        Mockito.verify(server).getRequest(query);
    }

    @Test(expected = ServerIsNotAvailable.class)
    public void shouldThrowExceptionGet() throws ServerIsNotAvailable {
        Mockito.when(server.isServerAvailable()).thenReturn(false);
        security.getRequest(query);
    }

}