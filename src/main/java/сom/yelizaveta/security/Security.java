package сom.yelizaveta.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import сom.yelizaveta.exception.ServerIsNotAvailable;
import сom.yelizaveta.model.Query;
import сom.yelizaveta.server.Server;

@Data
@AllArgsConstructor
public class Security {

    private Server server;

    public void sendRequest(Query query) throws ServerIsNotAvailable {
        if (server.isServerAvailable()) {
            server.sendRequest(query);
        } else throw new ServerIsNotAvailable("Oops");
    }

    public void getRequest(Query query) throws ServerIsNotAvailable {
        if (server.isServerAvailable()) {
            server.getRequest(query);
        } else throw new ServerIsNotAvailable("Oops");
    }
}
