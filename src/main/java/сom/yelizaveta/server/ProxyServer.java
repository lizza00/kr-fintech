package сom.yelizaveta.server;

import lombok.Data;
import сom.yelizaveta.model.Entry;
import сom.yelizaveta.model.Query;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class ProxyServer implements Server {

    List<Query> queries = new ArrayList<>();

    public List<Entry> countOfQueriesByAdresses() {
        /*Map<String, Integer> result = new HashMap<>();
        for (Query query : queries) {
            if (result.get(query.getSender()) == null) {
                result.put(query.getSender(), 1);
            } else {
                result.put(query.getSender(), result.get(query.getSender()));
            }
        }*/
        return queries.stream()
                .map(query -> new Entry(query.getSender(), queries.stream().filter(q -> q.getSender().equals(query.getSender())).count()))
                .distinct()
                .collect(Collectors.toList());
    }

    public String getTheMostPopularAdress() {
        /*Map<String, Integer> result = new TreeMap<>();
        for (Query query : queries) {
            if (result.get(query.getReceiver()) == null) {
                result.put(query.getReceiver(), 1);
            } else {
                result.put(query.getSender(), result.get(query.getSender()) + 1);
            }
        }*/
        return queries.stream()
                .map(query -> new Entry(query.getReceiver(), queries.stream().filter(q -> q.getReceiver().equals(query.getReceiver())).count()))
                .max(Entry::compareTo).get().getKey();
    }

    public long countOfRequestByAdressAndLastDay(String adress) {
        return queries.stream().filter(q -> q.getSender().equals(adress) && q.getTimeOfSending().isAfter(LocalDate.now().minusDays(1))).count();
    }

    @Override
    public void sendRequest(Query query) {
        System.out.println("Request is sent");
    }

    @Override
    public void getRequest(Query query) {
        System.out.println("Request is got");
    }

    @Override
    public boolean isServerAvailable() {
        return Math.random() % 3 == 0;
    }


}
