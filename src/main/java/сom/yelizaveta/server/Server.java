package сom.yelizaveta.server;

import сom.yelizaveta.model.Query;

public interface Server {
    void sendRequest(Query query);

    void getRequest(Query query);

    boolean isServerAvailable();
}
