package сom.yelizaveta.exception;

public class ServerIsNotAvailable extends Exception {

    public ServerIsNotAvailable(String message){
        super(message);
    }
}
