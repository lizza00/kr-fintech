package сom.yelizaveta.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Entry implements Comparable<Entry>{
    private String key;
    private long value;

    @Override
    public int compareTo(Entry o) {
        if(value>o.getValue()){
            return 1;
        }
        else if(value==o.getValue()){
            return 0;
        }
        return -1;
    }
}
